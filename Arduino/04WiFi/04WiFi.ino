#include<DHTesp.h>
#include<ESP8266WiFi.h>

const char* ssid="Zero's WiFi"; //nombre de la red
const char* password="qwertyuiop99";//contraseña de la red

DHTesp sensor;
TempAndHumidity valores;
int temp, hum;
void setup() {
  Serial.begin(9600);
  sensor.setup(D4,DHTesp::DHT11);
  delay(2000);
  WiFi.begin(ssid,password);
  Serial.print("Conectando a WiFi...");
  while(WiFi.status()!=WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("Ok! :)");
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  valores=sensor.getTempAndHumidity();
  temp=valores.temperature;
  hum=valores.humidity;
  Serial.println("Temp:" + String(temp) + " Hum:" + String(hum));
  delay(10000);
}
