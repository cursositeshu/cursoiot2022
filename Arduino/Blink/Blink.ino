#define pinLed D2

void setup() {
  pinMode(pinLed,OUTPUT);
}

void loop() {
  digitalWrite(pinLed,HIGH); //encendiendo el Led
  delay(500); //500ms= 0.5 segs esperamos medio segundo
  digitalWrite(pinLed,LOW);  //Apagando el led
  delay(500); //esperamos medio segundo
}
