#include<ESP8266WiFi.h>
#include<PubSubClient.h>
#define pinWiFi D0
const char* ssid="Zero's WiFi"; //nombre de la red
const char* password="qwertyuiop99";//contraseña de la red
const char* mqttServer="broker.hivemq.com";
const int mqttPort=1883;
char mqttTopic[]="CursoIoT/ProfeCarlos/Hardware";

WiFiClient wifiClientMqtt;
PubSubClient mqtt(wifiClientMqtt);

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
  pinMode(pinWiFi,OUTPUT);
  delay(2000);
  ConectarWiFi();
  ConectarMQTT();
}

void loop() {
  // put your main code here, to run repeatedly:

}

void ConectarMQTT(){
  Serial.println("Conectando a MQTT...");
  mqtt.setServer(mqttServer,mqttPort);
  mqtt.connect("ProfeCarlos");
  digitalWrite(pinWiFi,LOW);
  bool encendido=false;
  while(!mqtt.connected()){
    delay(500);
    Serial.print(".");
    if(encendido){
      digitalWrite(pinWiFi,LOW);
      encendido=false;
    }else{
      digitalWrite(pinWiFi,HIGH);
      encendido=true;
    }
  }
  
  digitalWrite(pinWiFi,HIGH);
  Serial.println("Ok!!!");
  mqtt.subscribe("Hardware");
  mqtt.setCallback(MensajeEntrante);
  //Serial.println("Suscrito a: CursoIoT/ProfeCarlos/Hardware");
}

void MensajeEntrante(char* topic, byte* message, unsigned int length){
  Serial.print(String(topic) + ": ");
  String msj="";
  for(int i=0;i<length;i++){
    Serial.print((char)message[i]);
    msj+=(char)message[i];
  }
  
}

void ConectarWiFi(){
  WiFi.begin(ssid,password);
  Serial.print("Conectando a WiFi...");
  digitalWrite(pinWiFi,LOW);
  bool encendido=false;
  while(WiFi.status()!=WL_CONNECTED){
    delay(500);
    Serial.print(".");
    if(encendido){
      digitalWrite(pinWiFi,LOW);
      encendido=false;
    }else{
      digitalWrite(pinWiFi,HIGH);
      encendido=true;
    }
  }
  digitalWrite(pinWiFi,HIGH);
  Serial.println("Ok! :)");
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
}
