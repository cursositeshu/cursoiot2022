#include<DHTesp.h>

DHTesp sensor;
TempAndHumidity valores;
int temp, hum;
void setup() {
  Serial.begin(9600);
  sensor.setup(D4,DHTesp::DHT11);
}

void loop() {
  valores=sensor.getTempAndHumidity();
  temp=valores.temperature;
  hum=valores.humidity;
  Serial.println("Temp:" + String(temp) + " Hum:" + String(hum));
  delay(2000);
}
