#define pinLed D2
#define pinLDR A0
#define pinPWM D5

int valor;
int pwm=0;

void setup() {
  Serial.begin(9600);
  pinMode(pinLed,OUTPUT);
  pinMode(pinLDR,INPUT);
}

void loop() {
  valor=analogRead(pinLDR);
  if(valor>=1000){
    digitalWrite(pinLed,HIGH);
  }else{
    digitalWrite(pinLed,LOW);
  }
  analogWrite(pinPWM,pwm);
  Serial.print("LDR:");
  Serial.print(valor);
  Serial.print(" ");
  Serial.print("PWM:");
  Serial.println(pwm);
  pwm+=10;
  if(pwm>255){
    pwm=0;
  }
  delay(200);
}
