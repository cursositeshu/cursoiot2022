﻿using LiteDB;
using MiCasaIoT.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MiCasaIoT.Servicios
{
    public class DALLocal
    {
        public string Error { get; set; }

        private readonly ConnectionString conexion = new ConnectionString() { Filename = "CasaIoT.DB" };

        public List<Lectura> ObtenerTodasLasLecturas
        {
            get
            {
                List<Lectura> datos;
                using(var db=new LiteDatabase(conexion))
                {
                    datos = db.GetCollection<Lectura>("Lectura").FindAll().ToList();
                }
                return datos;
            }
        }

        public bool InsertarLectura(Lectura lectura)
        {
            lectura.FechaHora = System.DateTime.Now;
            lectura.Id = Guid.NewGuid().ToString();
            string r;
            using(var db=new LiteDatabase(conexion))
            {
                r=db.GetCollection<Lectura>("Lectura").Insert(lectura);
            }
            if (r != null)
            {
                Error = "No se pudo insertar la lectura";
                return true;
            }
            else
            {
                Error = "";
                return false;
            }
        }
    }
}
