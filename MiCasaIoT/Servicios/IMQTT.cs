﻿using System;

namespace MiCasaIoT.Servicios
{
    public interface IMQTT
    {
        event EventHandler<string> MensajeRecibido;

        void EnviarComando(string comando);
    }
}