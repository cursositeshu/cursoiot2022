﻿using MiCasaIoT.Entidad;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace MiCasaIoT.Servicios
{
    public class DALNube
    {
        public string Error { get; set; }

        private readonly IMongoDatabase db;
        private IMongoCollection<Lectura> Lecturas() => db.GetCollection<Lectura>("Lectura");

        public DALNube()
        {
            db = new MongoClient(new MongoUrl("mongodb+srv://cespinoza:PtTLO3BnqGlJaH3w@clusteriot.nq1y001.mongodb.net/?retryWrites=true&w=majority")).GetDatabase("ClusterIoT");
        }

        public List<Lectura> ObtenerTodasLasLecturas
        {
            get
            {
                return Lecturas().AsQueryable().ToList();
            }
        }

        public bool InsertarLectura(Lectura lectura)
        {
            try
            {
                lectura.FechaHora = DateTime.Now;
                lectura.Id = Guid.NewGuid().ToString();
                Lecturas().InsertOne(lectura);
                Error = "";
                return true;
            }
            catch (Exception)
            {
                Error = "No se pudo insertar la lectura";
                return false;
            }
        }
    }
}
