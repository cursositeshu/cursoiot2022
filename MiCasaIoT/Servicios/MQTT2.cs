﻿using System;
using System.Threading;
using MQTTnet;
using MQTTnet.Client;
using System.Threading.Tasks;
using System.Text;
using System.Diagnostics;

namespace MiCasaIoT.Servicios
{
    public class MQTT2 
    {

        public string MqttServer { get; private set; }
        public string MqttTopic { get; private set; }
        public int MqttPort { get; private set; }
        public string NombreCliente { get; private set; }
        public long EventosTotales { get; private set; }
        public long MaxEventos { get; private set; }
        public long NumDesconexiones { get; set; }

        IMqttClient mqttClient;
        MqttClientOptions options;
        MqttFactory factory;

        public event EventHandler<string> MensajeRecibido;
        //public event EventHandler Conectado;
        //public event EventHandler Desconectado;
        //public event EventHandler<string> Error;

        public MQTT2()
        {
            MqttServer = "200.79.179.167";
            MqttTopic = "CursoIoT/ProfeCarlos/Plataforma";
            MqttPort = 1884;
            EventosTotales = 0;
            MaxEventos = 0;
            NumDesconexiones = 0;
            NombreCliente = Guid.NewGuid().ToString().Substring(5, 20);

            factory = new MqttFactory();
            mqttClient = factory.CreateMqttClient();
            mqttClient.ConnectedAsync += MqttClient_ConnectedAsync;
            mqttClient.DisconnectedAsync += MqttClient_DisconnectedAsync;
            mqttClient.ApplicationMessageReceivedAsync += MqttClient_ApplicationMessageReceivedAsync;
            options = new MqttClientOptionsBuilder()
               .WithTcpServer(MqttServer, MqttPort)
               .WithClientId(NombreCliente)
               .WithCleanSession(false)
               .Build();

            ConectarMQTT();
        }

        private async void ConectarMQTT()
        {
            Console.WriteLine($"{DateTime.Now.ToString()}: Conectando a MQTT Server:{MqttServer} Puerto:{MqttPort}...");
            await mqttClient.ConnectAsync(options, CancellationToken.None);
            var suscripcionOptions = factory.CreateSubscribeOptionsBuilder().WithTopicFilter(f => { f.WithTopic(MqttTopic); }).Build();
            while (!mqttClient.IsConnected)
            {
                Thread.Sleep(10);
            }
            var resonse = await mqttClient.SubscribeAsync(suscripcionOptions, CancellationToken.None);


        }

        private Task MqttClient_ApplicationMessageReceivedAsync(MqttApplicationMessageReceivedEventArgs arg)
        {
            EventosTotales++;
            if (EventosTotales == long.MaxValue)
            {
                EventosTotales = 1;
                MaxEventos++;
            }
            if (EventosTotales % 1000 == 0)
            {
                Debug.WriteLine(EventosTotales);
            }
            MensajeRecibido(this, Encoding.UTF8.GetString(arg.ApplicationMessage.Payload));

            return Task.CompletedTask;
        }

        private Task MqttClient_DisconnectedAsync(MqttClientDisconnectedEventArgs arg)
        {
            //Desconectado(this, null);
            NumDesconexiones++;
            Debug.WriteLine($"{DateTime.Now.ToString()}: Desconectado!!, reconectando...");
            ConectarMQTT();

            return Task.CompletedTask;
        }

        private Task MqttClient_ConnectedAsync(MqttClientConnectedEventArgs arg)
        {
            Debug.WriteLine($"{DateTime.Now.ToString()}: Conectado a MQTT !!!");
            //Conectado(this, null);
            return Task.CompletedTask;
        }

        public async void EnviarComando(string comando)
        {
            if (mqttClient.IsConnected)
            {
                await mqttClient.PublishAsync(new MqttApplicationMessage()
                {
                    Payload = Encoding.UTF8.GetBytes(comando),
                    QualityOfServiceLevel = MQTTnet.Protocol.MqttQualityOfServiceLevel.AtMostOnce,
                    Topic = "CursoIoT/ProfeCarlos/Hardware",
                    Retain = false
                });
            }
        }
    }
}
