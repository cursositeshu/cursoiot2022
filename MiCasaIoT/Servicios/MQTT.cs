﻿using OpenNETCF.MQTT;
using System;
using System.Text;

namespace MiCasaIoT.Servicios
{
    public class MQTT : IMQTT
    {
        MQTTClient mqtt;
        bool conectado = false;
        public event EventHandler<string> MensajeRecibido;
        Subscription subscription;

        public MQTT()
        {
            mqtt = new MQTTClient("200.79.179.167", 1884);
            mqtt.ReconnectPeriod = 1;
            mqtt.Connected += Mqtt_Connected;
            mqtt.Disconnected += Mqtt_Disconnected;
            mqtt.MessageReceived += Mqtt_MessageReceived;
            subscription = new Subscription("CursoIoT/ProfeCarlos/Plataforma");
            mqtt.Connect(Guid.NewGuid().ToString().Substring(5, 20));
        }

        private void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            MensajeRecibido(this, Encoding.UTF8.GetString(payload));
        }

        public void EnviarComando(string comando)
        {
            if (conectado)
            {
                mqtt.Publish("CursoIoT/ProfeCarlos/Hardware", Encoding.UTF8.GetBytes(comando), QoS.FireAndForget, false);
            }
        }

        private void Mqtt_Disconnected(object sender, EventArgs e)
        {
            conectado = false;
            mqtt.Disconnect();
            mqtt.Subscriptions.Remove(subscription);
            mqtt.Connect(Guid.NewGuid().ToString().Substring(5, 20));
        }

        private void Mqtt_Connected(object sender, EventArgs e)
        {
            conectado = true;
            mqtt.Subscriptions.Add(subscription);
        }
    }
}
