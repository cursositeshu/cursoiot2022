﻿using System;

namespace MiCasaIoT.Entidad
{
    public class Lectura
    {
        public string Id { get; set; }
        public DateTime FechaHora { get; set; }
        public float Temperatura { get; set; }
        public float Humedad { get; set; }
        public float Luminosidad { get; set; }
        public bool LucesEncendidas { get; set; }
    }
}
