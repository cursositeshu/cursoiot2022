﻿using MiCasaIoT.Entidad;
using MiCasaIoT.Servicios;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MiCasaIoT.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturaController : ControllerBase
    {
        //private string destino = "Local";
        private string destino = "Nube";
        DALLocal local;
        DALNube nube;

        public LecturaController()
        {
            local = new DALLocal();
            nube = new DALNube();
        }

        //[HttpGet]
        //public ActionResult<List<Lectura>> Get()
        //{
        //    if (destino == "Local")
        //    {
        //        return Ok(local.ObtenerTodasLasLecturas);
        //    }
        //    else
        //    {
        //        return Ok(nube.ObtenerTodasLasLecturas);
        //    }
        //}

        [HttpPost]
        public ActionResult Post(float temperatura,float humedad, float luminosidad, bool estatusLuces)
        {
            Lectura lectura = new Lectura()
            {
                Temperatura = temperatura,
                Humedad = humedad,
                Luminosidad = luminosidad,
                LucesEncendidas = estatusLuces
            };
            if (destino == "Local")
            {
                return Ok(local.InsertarLectura(lectura));
            }
            else
            {
                return Ok(nube.InsertarLectura(lectura));
            }
        }
    }
}
