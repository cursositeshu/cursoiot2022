﻿using OpenNETCF.MQTT;
using System;
using System.Text;

namespace SolucionIoT.Consola
{
    internal class Program
    {
        static MQTTClient mqtt;
        static void Main(string[] args)
        {
            mqtt = new MQTTClient("200.79.179.167", 1884);
            mqtt.Connect("ProfeCarlosConsola");
            mqtt.Connected += Mqtt_Connected;
            mqtt.MessageReceived += Mqtt_MessageReceived;
            string comando = "";
            do
            {
                Console.WriteLine("Comando a Enviar (enter para terminar):");
                comando = Console.ReadLine();
                if (comando != "")
                {
                    mqtt.Publish("CursoIoT/ProfeCarlos/Hardware", Encoding.UTF8.GetBytes(comando), QoS.FireAndForget, false);
                }
            } while (comando != "");
        }

        private static void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            Console.WriteLine(Encoding.UTF8.GetString(payload));
        }

        private static void Mqtt_Connected(object sender, EventArgs e)
        {
            Console.WriteLine("Conectado a MQTT");
            mqtt.Subscriptions.Add(new Subscription("CursoIoT/ProfeCarlos/Plataforma"));
        }
    }
}
