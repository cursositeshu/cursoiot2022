/*
 Name:		Prototipo02.ino
 Created:	28/06/2022 13:15:58
 Author:	Carlos Espinoza  cespinoza@iteshu.edu.mx

  Comandos:
  L1=Encender las luces
  L0=Apagar las luces
  ?L=Regresa el estatus de las luces
  ?V=Obtiene los datos de los valores
  ?T=Obtiene la temperatura
  ?H=Obtiene la humedad
  ?U=Obtiene la luminosidad
  SXXX=mueve el servo motor a ese �ngulo
*/
#include<DHTesp.h>
#include<ESP8266WiFi.h>
#include<PubSubClient.h>
#include<ThingSpeak.h>
#include<Servo.h>


#define pinLDR A0
#define pinWiFi D0
#define pinLuces D1
#define pinDHT11 D4
#define pinServo D8
#define AIO_USERNAME  "ProfeCarlos"
#define AIO_KEY       "aio_GKyG23yNfWgoL2WMCbAoScRXl9Qp"
#define AIO_SERVER    "io.adafruit.com"
#define AIO_PORT      1883

const char* ssid = "Zero's WiFi"; //nombre de la red
const char* password = "qwertyuiop99";//contrase�a de la red
const char* mqttServer = "200.79.179.167";
const int mqttPort = 1884;
char mqttTopic[] = "CursoIoT/ProfeCarlos/Hardware";
unsigned long channelId = 1775435;
const char* apiKey = "Y9KQHCYK8C51KHZ6";
const char* userIoTCloud = "cespinoza@iteshu.edu.mx";
const char* passIoTCloud = "123456";
char mqttIoTCloud[] = "62b32b601dfb570580504f88";

WiFiClient wifiClientMqtt;
WiFiClient wifiClientAIO;
WiFiClient wifiIoTCloud;
WiFiClient wifiAPI;
PubSubClient mqtt(wifiClientMqtt);
PubSubClient aio(wifiClientAIO);
DHTesp sensor;
WiFiClient clientTS;
Servo servo;


int temp, hum;
int lum;
bool lucesEncendidas = false;
String comando = "";
unsigned long tiempo, tiempoTS, tiempoAIO, tiempoCargar;


void setup() {
	Serial.begin(9600);
	sensor.setup(pinDHT11, DHTesp::DHT11);
	pinMode(pinLDR, INPUT);
	pinMode(pinWiFi, OUTPUT);
	pinMode(pinLuces, OUTPUT);
	servo.attach(pinServo);
	delay(2000);
	ConectarWiFi();
	ConectarMQTT();
	ConectarAIO();
	Sensar();
	EnviarDatos();
	ThingSpeak.begin(clientTS);
	tiempo = millis();
	tiempoTS = millis();
	tiempoAIO = millis();
	tiempoCargar = millis();
	servo.write(0);
}

void loop() {
	RevisarConexiones();
	mqtt.loop();
	aio.loop();
	if (Serial.available()) {
		comando = Serial.readString();
		comando = comando.substring(0, comando.length() - 1);
		Serial.println("[" + comando + "]");
	}
	ProcesarComando();
	Sensar();
	CargarTS();
	CargarAIO();
	CargarEnAPI();
}

void RevisarConexiones() {
	//Si se pierde la conexi�n WiFi
	if (WiFi.status() != WL_CONNECTED) {
		ESP.reset();
	}
	//Si se desconecta de MQTT
	if (!mqtt.connected()) {
		ConectarMQTT();
	}
	//Si se desconecta de Adafruit.IO
	if (!aio.connected())
	{
		ConectarAIO();
	}
	//El valor maximo de millis es:4294967295 aprox 49.71 dias
	//https://www.best-microcontroller-projects.com/arduino-millis.html
	//Si se acerca a 49 d�as, reiniciar el sistema
	if (millis() >= 4294967000 || millis() == 0) {
		ESP.reset();
	}
}

void CargarEnAPI() {
	if ((millis() - tiempoCargar) > (1000 * 60 * 0.5))
	{
		Serial.println();
		Serial.println("Cargando datos en API...");
		if (!wifiAPI.connect("cursoiotprofecarlos.somee.com", 80)) {
			Serial.println("Error al conectar con la API");
			return;
		}
		if (lucesEncendidas) {
			wifiAPI.print(String("POST /api/Lectura?temperatura=") + String(temp) + "&humedad="
				+ String(hum) + "&luminosidad=" + String(lum) + "&estatusLuces=true" + " HTTP/1.1\r\nHost: cursoiotprofecarlos.somee.com\r\nContent-Type: "
				+ "application/json; charset=utf-8\r\nContent-Length:0\r\nConnection: close\r\n\r\n");
		}
		else {
			wifiAPI.print(String("POST /api/Lectura?temperatura=") + String(temp) + "&humedad="
				+ String(hum) + "&luminosidad=" + String(lum) + "&estatusLuces=false" + " HTTP/1.1\r\nHost: cursoiotprofecarlos.somee.com\r\nContent-Type: "
				+ "application/json; charset=utf-8\r\nContent-Length:0\r\nConnection: close\r\n\r\n");
		}
		delay(2000);
		while (wifiAPI.available()) {
			char c = wifiAPI.read();
			Serial.print(c);
		}
		tiempoCargar = millis();
	}
}

void CargarEnIoTCloud(char* idSensor, float valor) {
	if (!wifiIoTCloud.connect("iotcloud2019.azurewebsites.net", 80)) {
		Serial.println("Conexi�n fallida");
		return;
	}
	wifiIoTCloud.print(String("POST ") + "/api/Lectura?user=" + String(userIoTCloud) +
		"&pass=" + String(passIoTCloud) + "&idSensor=" + String(idSensor) + "&valor=" +
		String(valor) + " HTTP/1.1\r\nHost: iotcloud2019.azurewebsites.net\r\nContent-Type: "
		+ "application/json; charset=utf-8\r\nContent-Length:0\r\nConnection: close\r\n\r\n");
	delay(2000);
	while (wifiIoTCloud.available()) {
		char c = wifiIoTCloud.read();
		Serial.print(c);
	}
}

void ConectarAIO() {
	Serial.println("Conectando a Adafruit IO...");
	aio.setServer(AIO_SERVER, AIO_PORT);
	aio.connect("CursoIoTProfeCarlosHardware", AIO_USERNAME, AIO_KEY);
	digitalWrite(pinWiFi, LOW);
	bool encendido = false;
	while (!aio.connected()) {
		delay(500);
		Serial.print(".");
		if (encendido) {
			digitalWrite(pinWiFi, LOW);
			encendido = false;
		}
		else {
			digitalWrite(pinWiFi, HIGH);
			encendido = true;
		}
	}
	aio.setCallback(MensajeEntrante);
	digitalWrite(pinWiFi, HIGH);
	Serial.println("Ok!!!");
	aio.subscribe("ProfeCarlos/feeds/Luces");
}

void CargarAIO() {
	if ((millis() - tiempoAIO) > 30000) {
		PublicarAIO("ProfeCarlos/feeds/temp", String(temp));
		PublicarAIO("ProfeCarlos/feeds/hum", String(hum));
		PublicarAIO("ProfeCarlos/feeds/lum", String(lum));
		if (lucesEncendidas) {
			PublicarAIO("ProfeCarlos/feeds/lucesencendidas", "on");
		}
		else {
			PublicarAIO("ProfeCarlos/feeds/lucesencendidas", "off");
		}
		tiempoAIO = millis();
		Serial.println("Datos cargados a Adafuit IO");
	}
}

void PublicarAIO(char* tema, String texto) {
	char buf[50];
	texto.toCharArray(buf, 50);
	aio.publish(tema, buf);
}

void CargarTS() {
	if ((millis() - tiempoTS) > 30000) {
		ThingSpeak.setField(1, temp);
		ThingSpeak.setField(2, hum);
		ThingSpeak.setField(3, lum);
		if (lucesEncendidas) {
			ThingSpeak.setField(4, 1);
		}
		else {
			ThingSpeak.setField(4, 0);
		}
		int r = ThingSpeak.writeFields(channelId, apiKey);
		Serial.println();
		Serial.println();
		Serial.print("\n\rCargando a ThingSpeak...");
		if (r == 200) {
			Serial.println("Correcto!!!");
		}
		else {
			Serial.println("Error!!!");
		}
		tiempoTS = millis();
	}
}

void ProcesarComando() {

	if (comando != "") {
		if (comando == "L1") {
			EncenderLuces();
		}
		if (comando == "L0") {
			ApagarLuces();
		}
		if (comando == "?L") {
			EnviarRespuesta(String(lucesEncendidas));
		}
		if (comando == "?V") {
			EnviarRespuesta(String(temp) + "|" + String(hum) + "|" + String(lum));
		}
		if (comando == "?T") {
			EnviarRespuesta(String(temp));
		}
		if (comando == "?H") {
			EnviarRespuesta(String(hum));
		}
		if (comando == "?U") {
			EnviarRespuesta(String(lum));
		}
		if (comando[0] == 'S') {
			int valor = comando.substring(1).toInt();
			servo.write(valor);
			EnviarRespuesta("Angulo: " + String(valor));
		}
		comando = "";
	}
}

void EnviarRespuesta(String texto) {
	Serial.println(texto);
	char buf[50];
	texto.toCharArray(buf, 50);
	mqtt.publish("CursoIoT/ProfeCarlos/Plataforma", buf);
}

void EnviarDatos() {
	//C�digo para cargar los datos a la API
	Serial.println();
	Serial.println();
	Serial.println("Enviando Datos a API...");
	CargarEnIoTCloud("62b32be21dfb570580504f8a", temp);
	CargarEnIoTCloud("62b32c1a1dfb570580504f8b", hum);
	CargarEnIoTCloud("62b32bb81dfb570580504f89", lum);
	CargarEnAPI();
}

void Sensar() {
	if ((millis() - tiempo) > 2000) {
		TempAndHumidity valores = sensor.getTempAndHumidity();
		temp = valores.temperature;
		hum = valores.humidity;
		lum = analogRead(pinLDR);
		tiempo = millis();
		//Serial.println("Temp:" + String(temp) + " Hum:" + String(hum) + " Lum:" + String(lum));	
	}
}

void ConectarMQTT() {
	Serial.println("Conectando a MQTT...");
	mqtt.setServer(mqttServer, mqttPort);
	mqtt.connect("CursoIoTProfeCarlosHardware");
	digitalWrite(pinWiFi, LOW);
	bool encendido = false;
	while (!mqtt.connected()) {
		delay(500);
		Serial.print(".");
		if (encendido) {
			digitalWrite(pinWiFi, LOW);
			encendido = false;
		}
		else {
			digitalWrite(pinWiFi, HIGH);
			encendido = true;
		}
	}
	mqtt.setCallback(MensajeEntrante);
	digitalWrite(pinWiFi, HIGH);
	Serial.println("Ok!!!");
	EnviarRespuesta("Hardware Conectado");
	mqtt.subscribe(mqttTopic);
	Serial.println("Suscrito a: CursoIoT/ProfeCarlos/Hardware");
	mqtt.subscribe(mqttIoTCloud);
	Serial.println("Suscrito a: IoTCloud");
}

void MensajeEntrante(char* topic, byte* message, unsigned int length) {
	Serial.print(String(topic) + ": ");
	String msj = "";
	for (int i = 0; i < length; i++) {
		Serial.print((char)message[i]);
		msj += (char)message[i];
	}
	comando = msj;
}

void ConectarWiFi() {
	WiFi.begin(ssid, password);
	Serial.print("Conectando a WiFi...");
	digitalWrite(pinWiFi, LOW);
	bool encendido = false;
	
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
		if (encendido) {
			digitalWrite(pinWiFi, LOW);
			encendido = false;
		}
		else {
			digitalWrite(pinWiFi, HIGH);
			encendido = true;
		}
	}
	
	digitalWrite(pinWiFi, HIGH);
	Serial.println("Ok! :)");
	Serial.print("IP: ");
	Serial.println(WiFi.localIP());
}

void EncenderLuces() {
	digitalWrite(pinLuces, HIGH);
	lucesEncendidas = true;
	CargarEnIoTCloud("62b32c591dfb570580504f8c", 1);
}

void ApagarLuces() {
	digitalWrite(pinLuces, LOW);
	lucesEncendidas = false;
	CargarEnIoTCloud("62b32c591dfb570580504f8c", 0);
}

